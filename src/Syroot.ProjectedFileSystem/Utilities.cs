﻿using System;

namespace Syroot.ProjectedFileSystem
{
    internal static class Utilities
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static long ToFileTimeUtcOrNull(this DateTime self) => self.Ticks == 0 ? 0 : self.ToFileTimeUtc();
    }
}
