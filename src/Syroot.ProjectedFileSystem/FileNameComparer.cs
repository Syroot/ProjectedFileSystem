﻿using System.Collections.Generic;
using static Syroot.ProjectedFileSystem.WinApi.ProjectedFSLib;

namespace Syroot.ProjectedFileSystem
{
    /// <summary>
    /// Represents a string comparer comparing two file or directory names and returning a value that indicates their
    /// relative collation order.
    /// </summary>
    public class FileNameComparer : IComparer<string>
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public int Compare(string x, string y)
        {
            return PrjFileNameCompare(x, y);
        }
    }
}
