﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using static Syroot.ProjectedFileSystem.WinApi.ProjectedFSLib;

namespace Syroot.ProjectedFileSystem
{
    public abstract class FileSystemProvider : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Guid _guid;
        private IntPtr _instanceHandle;
        private readonly Dictionary<Guid, EnumerationInfo> _enumerations;
        private readonly PrjStartDirectoryEnumerationCb _startDirectoryEnumerationCb;
        private readonly PrjEndDirectoryEnumerationCb _endDirectoryEnumerationCb;
        private readonly PrjGetDirectoryEnumerationCb _getDirectoryEnumerationCb;
        private readonly PrjGetPlaceholderInfoCb _getPlaceholderInfoCb;
        private readonly PrjGetFileDataCb _getFileDataCb;
        private readonly PrjNotificationCb _notificationCb;
        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public FileSystemProvider()
        {
            _guid = Guid.NewGuid();
            _enumerations = new Dictionary<Guid, EnumerationInfo>();

            _startDirectoryEnumerationCb = StartDirectoryEnumerationCb;
            _endDirectoryEnumerationCb = EndDirectoryEnumerationCb;
            _getDirectoryEnumerationCb = GetDirectoryEnumerationCb;
            _getPlaceholderInfoCb = GetPlaceholderInfoCb;
            _getFileDataCb = GetFileDataCb;
            _notificationCb = NotificationCb;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public bool IsProjecting { get; private set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void StartProjecting(string rootPathName)
        {
            if (IsProjecting)
                throw new InvalidOperationException("File system provider is already projecting.");

            IsProjecting = true;
            Marshal.ThrowExceptionForHR(PrjMarkDirectoryAsPlaceholder(rootPathName, null, IntPtr.Zero, ref _guid));

            // Point at the callbacks with the callback table.
            PrjCallbacks callbackTable = new PrjCallbacks
            {
                StartDirectoryEnumerationCallback = _startDirectoryEnumerationCb,
                EndDirectoryEnumerationCallback = _endDirectoryEnumerationCb,
                GetDirectoryEnumerationCallback = _getDirectoryEnumerationCb,
                GetPlaceholderInfoCallback = _getPlaceholderInfoCb,
                GetFileDataCallback = _getFileDataCb,
                NotificationCallback = _notificationCb
            };
            // Start the instance.
            Marshal.ThrowExceptionForHR(PrjStartVirtualizing(rootPathName, ref callbackTable, IntPtr.Zero, IntPtr.Zero,
                ref _instanceHandle));
        }

        public void StopProjecting()
        {
            if (!IsProjecting)
                return;

            IsProjecting = false;
            PrjStopVirtualizing(_instanceHandle);
            _enumerations.Clear();
        }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing).
            Dispose(true);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected static int CompareFileName(string a, string b)
        {
            return PrjFileNameCompare(a, b);
        }

        protected static bool MatchFileName(string fileName, string pattern)
        {
            if (pattern == null)
                Debugger.Break();

            return pattern == null || PrjFileNameMatch(fileName, pattern);
        }

        protected abstract IEnumerable<FileSystemEntry> GetEntries(string virtualPath, string pattern, bool returnSingle);

        protected abstract FileSystemEntry GetEntry(string virtualPath);

        protected abstract Stream GetFileStream(string virtualPath);

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                    StopProjecting();

                _disposed = true;
            }
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void LogCallback(string callback, string filePathName, Guid? enumerationId)
        {
            StringBuilder sb = new StringBuilder();
            if (enumerationId.HasValue)
                sb.Append($"[{enumerationId.ToString().Substring(0, 8)}] ");
            sb.Append($"{callback} ");
            if (filePathName != null)
                sb.Append($"\"{filePathName}\"");
            Console.WriteLine(sb.ToString());
        }

        private static void ReadChunks(Stream source, Span<byte> chunk, long length, Action<int> callback = null)
        {
            while (length > 0)
            {
                int bytesRead = source.Read(chunk);
                if (bytesRead == 0)
                    throw new IOException("Cannot read required number of bytes.");
                callback?.Invoke(bytesRead);
                length -= bytesRead;
            }
        }

        private static void SeekSafe(Stream stream, long offset, Span<byte> buffer)
        {
            if (offset == 0)
                return;
            else if (stream.CanSeek)
                stream.Position = offset;
            else
                ReadChunks(stream, buffer, offset);
        }

        // ---- OS Callbacks ----

        private int StartDirectoryEnumerationCb(IntPtr pCallbackData, ref Guid enumerationId)
        {
            PrjCallbackData callbackData = Marshal.PtrToStructure<PrjCallbackData>(pCallbackData);
            LogCallback("ENUM_START", callbackData.FilePathName, enumerationId);

            _enumerations.Add(enumerationId, new EnumerationInfo());

            return 0;
        }

        private int GetDirectoryEnumerationCb(IntPtr pCallbackData, ref Guid enumerationId, string searchExpression,
            IntPtr dirEntryBufferHandle)
        {
            PrjCallbackData callbackData = Marshal.PtrToStructure<PrjCallbackData>(pCallbackData);
            LogCallback("ENUM_GET", callbackData.FilePathName, enumerationId);

            bool restartScan = callbackData.Flags.HasFlag(PrjCallbackDataFlags.RestartScan);
            bool returnSingleEntry = callbackData.Flags.HasFlag(PrjCallbackDataFlags.ReturnSingleEntry);

            // Ensure the enumeration is initialized.
            EnumerationInfo enumeration = _enumerations[enumerationId];
            if (enumeration.Enumerator == null || restartScan)
                enumeration.Enumerator = GetEntries(callbackData.FilePathName, searchExpression, returnSingleEntry).GetEnumerator();
            IEnumerator<FileSystemEntry> enumerator = enumeration.Enumerator;

            if (enumerator.MoveNext())
            {
                FileSystemEntry entry = enumerator.Current;
                PrjFileBasicInfo fileBasicInfo = entry.GetPrjFileBasicInfo();
                PrjFillDirEntryBuffer(entry.Name, ref fileBasicInfo, dirEntryBufferHandle);
            }

            return 0;
        }

        private int EndDirectoryEnumerationCb(IntPtr pCallbackData, ref Guid enumerationId)
        {
            LogCallback("ENUM_END", null, enumerationId);

            if (!_enumerations.Remove(enumerationId))
                throw new InvalidOperationException("Removed non-existant enumeration.");

            return 0;
        }

        private int GetPlaceholderInfoCb(IntPtr pCallbackData)
        {
            PrjCallbackData callbackData = Marshal.PtrToStructure<PrjCallbackData>(pCallbackData);
            LogCallback("PLACEHOLDER", callbackData.FilePathName, null);

            // Try to get an entry for the given name.
            FileSystemEntry entry = GetEntry(callbackData.FilePathName);
            if (entry == null)
            {
                return Marshal.GetHRForException(new FileNotFoundException());
            }
            else
            {
                // Return the retrieved entry information and write it back to the OS.
                PrjPlaceholderInfo placeholderInfo = new PrjPlaceholderInfo
                {
                    FileBasicInfo = entry.GetPrjFileBasicInfo(),
                    VersionInfo = new PrjPlaceholderVersionInfo(),
                    VariableData = new byte[] { 0 }
                };
                Marshal.ThrowExceptionForHR(PrjWritePlaceholderInfo(callbackData.NamespaceVirtualizationContext,
                    entry.FullPathName, ref placeholderInfo, (uint)Marshal.SizeOf(placeholderInfo)));
                return 0;
            }

        }

        private int GetFileDataCb(IntPtr pCallbackData, ulong byteOffset, uint length)
        {
            const int _bufferSize = 1024 * 1024;

            PrjCallbackData callbackData = Marshal.PtrToStructure<PrjCallbackData>(pCallbackData);
            LogCallback("FILEDATA", callbackData.FilePathName, null);

            Stream stream = null;
            try
            {
                // Retrieve a stream to the file as implemented by the provider.
                stream = GetFileStream(callbackData.FilePathName);
                if (stream == null)
                    return Marshal.GetHRForException(new FileNotFoundException());

                // Create a buffer for seek / file data.
                using (DataBuffer dataBuffer = new DataBuffer(callbackData.NamespaceVirtualizationContext, _bufferSize))
                {
                    // Safely seek to a requested offset.
                    Span<byte> buffer = dataBuffer.AsSpan();
                    SeekSafe(stream, (long)byteOffset, buffer);

                    // Write the file data in chunks back to the OS.
                    ReadChunks(stream, buffer, length, bytesRead =>
                    {
                        PrjWriteFileData(callbackData.NamespaceVirtualizationContext, ref callbackData.DataStreamId,
                            dataBuffer.Address, byteOffset, (uint)bytesRead);
                        byteOffset += (uint)bytesRead;
                    });
                }

                return 0;
            }
            finally
            {
                stream?.Dispose();
            }
        }

        private int NotificationCb(IntPtr pCallbackData, bool isDirectory, PrjNotification notification,
            string destinationFileName, ref PrjNotificationParameters operationParameters)
        {
            PrjCallbackData callbackData = Marshal.PtrToStructure<PrjCallbackData>(pCallbackData);
            LogCallback("NOTIFICATION", callbackData.FilePathName, null);

            return 0;
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private class EnumerationInfo
        {
            internal IEnumerator<FileSystemEntry> Enumerator { get; set; }
        }
    }
}
