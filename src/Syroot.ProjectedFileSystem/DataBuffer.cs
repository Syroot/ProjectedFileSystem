﻿using System;
using static Syroot.ProjectedFileSystem.WinApi.ProjectedFSLib;

namespace Syroot.ProjectedFileSystem
{
    /// <summary>
    /// Represents an aligned file data buffer.
    /// </summary>
    internal class DataBuffer : IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _disposed;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal DataBuffer(IntPtr namespaceVirtualizationContext, int size)
        {
            Address = PrjAllocateAlignedBuffer(namespaceVirtualizationContext, (uint)size);
            if (Address == IntPtr.Zero)
                throw new OutOfMemoryException("Could not allocate aligned buffer.");
            Size = size;
        }

        ~DataBuffer()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing).
            Dispose(false);
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing).
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal IntPtr Address { get; private set; }

        internal int Size { get; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal Span<byte> AsSpan()
        {
            if (Address == IntPtr.Zero)
                throw new InvalidOperationException("Buffer not initialized.");

            unsafe { return new Span<byte>((byte*)Address, Size); }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                // Dispose managed state (managed objects).
                if (disposing) { }

                // Free unmanaged resources (unmanaged objects). Set large fields to null.
                if (Address != IntPtr.Zero)
                {
                    PrjFreeAlignedBuffer(Address);
                    Address = IntPtr.Zero;
                }

                _disposed = true;
            }
        }
    }
}
