﻿using System;
using System.IO;
using static Syroot.ProjectedFileSystem.WinApi.ProjectedFSLib;

namespace Syroot.ProjectedFileSystem
{
    public class FileSystemEntry
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public FileSystemEntry(string name, string fullPathName, bool isDirectory)
        {
            Name = name;
            FullPathName = fullPathName;
            IsDirectory = isDirectory;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; }
        public string FullPathName { get; set; }
        public bool IsDirectory { get; set; }
        public long FileSize { get; set; }
        public DateTime CreationTimeUtc { get; set; }
        public DateTime LastAccessTimeUtc { get; set; }
        public DateTime LastWriteTimeUtc { get; set; }
        public DateTime ChangeTimeUtc { get; set; }
        public FileAttributes Attributes { get; set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal PrjFileBasicInfo GetPrjFileBasicInfo() => new PrjFileBasicInfo
        {
            IsDirectory = IsDirectory,
            FileSize = FileSize,
            CreationTime = CreationTimeUtc.ToFileTimeUtcOrNull(),
            LastAccessTime = LastAccessTimeUtc.ToFileTimeUtcOrNull(),
            LastWriteTime = LastWriteTimeUtc.ToFileTimeUtcOrNull(),
            ChangeTime = ChangeTimeUtc.ToFileTimeUtcOrNull(),
            FileAttributes = Attributes
        };
    }
}
