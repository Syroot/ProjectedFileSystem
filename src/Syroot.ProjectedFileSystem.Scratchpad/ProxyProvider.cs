﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Syroot.ProjectedFileSystem.Scratchpad
{
    internal class ProxyProvider : FileSystemProvider
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly string _sourceDirectory;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal ProxyProvider(string sourceDirectory)
        {
            _sourceDirectory = sourceDirectory;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override IEnumerable<FileSystemEntry> GetEntries(string virtualPath, string pattern, bool returnSingle)
        {
            // Retrieve all directory entries in the same order as the file system.
            foreach (string entry in Directory.GetFileSystemEntries(GetSourcePath(virtualPath))
                .Where(x => MatchFileName(x, pattern))
                .OrderBy(x => x, new FileNameComparer()))
            {
                yield return CreateFileSystemEntry(entry);
            }
        }

        protected override FileSystemEntry GetEntry(string virtualPath)
        {
            string virtualDirectory = Path.GetDirectoryName(virtualPath);

            string entry = Directory.GetFileSystemEntries(GetSourcePath(virtualDirectory))
                .FirstOrDefault(x => CompareFileName(Path.GetFileName(x), Path.GetFileName(virtualPath)) == 0);

            if (entry != null)
                return CreateFileSystemEntry(entry);

            return null;
        }

        protected override Stream GetFileStream(string virtualPath)
        {
            string sourcePath = GetSourcePath(virtualPath);
            return File.Exists(sourcePath) ? new FileStream(sourcePath, FileMode.Open) : null;
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private FileSystemEntry CreateFileSystemEntry(string entry)
        {
            string name = Path.GetFileName(entry);
            string fullPathName = GetVirtualPath(entry);

            if (Directory.Exists(entry))
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(entry);
                return new FileSystemEntry(name, fullPathName, true)
                {
                    Attributes = directoryInfo.Attributes,
                    ChangeTimeUtc = directoryInfo.LastWriteTimeUtc,
                    CreationTimeUtc = directoryInfo.CreationTimeUtc,
                    LastAccessTimeUtc = directoryInfo.LastAccessTimeUtc,
                    LastWriteTimeUtc = directoryInfo.LastWriteTimeUtc
                };
            }
            else
            {
                FileInfo fileInfo = new FileInfo(entry);
                return new FileSystemEntry(name, fullPathName, false)
                {
                    Attributes = fileInfo.Attributes,
                    ChangeTimeUtc = fileInfo.LastWriteTimeUtc,
                    CreationTimeUtc = fileInfo.CreationTimeUtc,
                    LastAccessTimeUtc = fileInfo.LastAccessTimeUtc,
                    LastWriteTimeUtc = fileInfo.LastWriteTimeUtc,
                    FileSize = fileInfo.Length
                };
            }
        }

        private string GetSourcePath(string virtualPath) => Path.Combine(_sourceDirectory, virtualPath);

        private string GetVirtualPath(string sourcePath) => sourcePath.StartsWith(_sourceDirectory)
            ? sourcePath.Substring(_sourceDirectory.Length).TrimStart(new[] { Path.DirectorySeparatorChar })
            : sourcePath;
    }
}
