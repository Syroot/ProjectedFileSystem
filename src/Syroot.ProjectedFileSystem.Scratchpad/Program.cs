﻿using System;
using System.IO;
using System.Threading;

namespace Syroot.ProjectedFileSystem.Scratchpad
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            // Create empty dummy directory to project to.
            const string directory = @"C:\test";
            if (Directory.Exists(directory))
            {
                Directory.Delete(directory, true);
                while (Directory.Exists(directory))
                    Thread.Sleep(100);
            }
            Directory.CreateDirectory(directory);

            // Start a sample provider projecting into that directory.
            using (ProxyProvider provider = new ProxyProvider(@"D:\Downloads"))
            {
                provider.StartProjecting(directory);

                Console.WriteLine("Press any key to to stop the projected file system provider...");
                Console.Read();
            }
        }
    }
}
