# Projected File System

This .NET library is a (currently experimental) C# wrapper around the
[Projected File System (ProjFS) functionality](https://docs.microsoft.com/en-us/windows/desktop/projfs/projected-file-system)
introduced in Windows 10 1809.

ProjFS allows you to "inject" virtual / made-up folders and files into the file system. The motivation behind it
originates from mirroring Git repositories on your hard disk without requiring to actually store the contents on your
local drive.

